from django.shortcuts import render
from django.views.generic import View

# Create your views here.
from .models import App1Models

class App1View(View):
    def get(self, request):
        data = App1Models.objects.all()
        context = {'data': data}
        return render(request, 'app1/app1.html', context=context)