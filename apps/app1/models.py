from django.db import models

# Create your models here.

class App1Models(models.Model):
    objects = models.Manager()
    name = models.CharField(max_length = 150)
    text = models.TextField()
    def __str__(self):
        return self.name