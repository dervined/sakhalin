from django.contrib import admin
from django.urls import path, include

from .views import App1View

urlpatterns = [
    path('', App1View.as_view(), name='app1')
]
